package Practica5;

import java.util.Random;
import javax.swing.JOptionPane;

public class practica5 extends javax.swing.JFrame {

    /**
     * Creates new form practica5
     */
    public practica5() {
        initComponents();
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        limiteinfe = new javax.swing.JSpinner();
        limitesupe = new javax.swing.JSpinner();
        tfnumerogenerado = new javax.swing.JTextField();
        btngenerador = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jLabel1.setText("limite inferior");

        jLabel2.setText("limite superiror");

        jLabel3.setText("numero generado");

        limiteinfe.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                limiteinfeStateChanged(evt);
            }
        });

        limitesupe.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                limitesupeStateChanged(evt);
            }
        });

        tfnumerogenerado.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        tfnumerogenerado.setText("0");

        btngenerador.setText("Generar numero");
        btngenerador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btngeneradorActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(73, 73, 73)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(limiteinfe, javax.swing.GroupLayout.DEFAULT_SIZE, 57, Short.MAX_VALUE)
                                    .addComponent(limitesupe)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(50, 50, 50)
                                .addComponent(tfnumerogenerado, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(88, 88, 88)
                        .addComponent(btngenerador)))
                .addContainerGap(102, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(limiteinfe, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(54, 54, 54)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(limitesupe, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(60, 60, 60)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(tfnumerogenerado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addComponent(btngenerador)
                .addContainerGap(37, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btngeneradorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btngeneradorActionPerformed
       int min = 0;
        int max = 0;
        int numGen = 0;
       
        Random random = new Random();
       
        min = (int) this.limiteinfe.getValue();
        max = (int) this.limitesupe.getValue();
           
        try{
          numGen = random.nextInt(max-min) + min; 
          } catch (Exception e){
            JOptionPane.showMessageDialog(rootPane, "Tiene que establecer un rango mayor a 0", "Error", JOptionPane.ERROR_MESSAGE);
        }
 
        this.tfnumerogenerado.setText(Integer.toString(numGen));
    }//GEN-LAST:event_btngeneradorActionPerformed

    private void limiteinfeStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_limiteinfeStateChanged
         int min = 0;
       int max = 0;
    
        min = (int) this.limiteinfe.getValue();
       max = (int) this.limitesupe.getValue();
       
       if (min>=max){
        JOptionPane.showMessageDialog(rootPane, "El valor del Limite Inferior debe ser menor al Limite Superior", "Error", JOptionPane.ERROR_MESSAGE);
       this.limiteinfe.setValue(max-1);
       }
    }//GEN-LAST:event_limiteinfeStateChanged

    private void limitesupeStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_limitesupeStateChanged
       int min = 0;
       int max = 0;
    
        min = (int) this.limiteinfe.getValue();
        max = (int) this.limitesupe.getValue();
       
       if (min>=max){
        JOptionPane.showMessageDialog(rootPane, "El valor del Limite Inferior debe ser menor al Limite Superior", "Error", JOptionPane.ERROR_MESSAGE);
       this.limitesupe.setValue(min-1);
    }          
    }//GEN-LAST:event_limitesupeStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(practica5.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(practica5.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(practica5.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(practica5.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new practica5().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btngenerador;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JSpinner limiteinfe;
    private javax.swing.JSpinner limitesupe;
    private javax.swing.JTextField tfnumerogenerado;
    // End of variables declaration//GEN-END:variables
}
