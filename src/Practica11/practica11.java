package Practica11;

import java.awt.Component;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

public class practica11 extends javax.swing.JFrame {
    Connection conn = null;
    DefaultTableModel tm = null;
    int rengSel=0;

    public practica11() {
       initComponents();
       
       this.jTable1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
       
       tm = (DefaultTableModel) this.jTable1.getModel();
       
        try {
            this.loadDriver();
        } catch (Exception ex) {
            // handle the error
            JOptionPane.showMessageDialog(rootPane, "No se pudo cargar el driver MySQL:"+ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        };
        this.connectToBD();
        this.consultar();
    }

     void loadDriver() throws Exception {
         Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
         System.out.println("Driver cargo exitosamente");
    }
    
     void connectToBD(){
         try {
      conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/encuesta?zeroDateTimeBehavior=CONVERT_TO_NULL" +
                                   "&user=encuesta_user&password=encuesta_pass");

 

       System.out.println("Conectado!");
   
      } catch (SQLException ex) {
    
     JOptionPane.showMessageDialog(rootPane, "Error en la conexión a la BD:"+ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      
    System.out.println("SQLException: " + ex.getMessage());
    System.out.println("SQLState: " + ex.getSQLState());
    System.out.println("VendorError: " + ex.getErrorCode());
    
     System.exit(0);
            }   
      }
    
     
      void consultar(){
        String sQuery = "SELECT id, sSisOper, Progra, Diseno, Admon, Horas FROM respuestas";
        ResultSet rset;
       
        try {
              Statement stmt = this.conn.createStatement();
           
              rset = stmt.executeQuery(sQuery);
             
              tm.setRowCount(0);
             
              while(rset.next()){
                  tm.addRow(new Object[]{rset.getInt("id"),
                                         rset.getInt("sSisOper"),
                                         rset.getString("cProgra"),
                                         rset.getString("cDiseno"),
                                         rset.getString("cAdmon"),
                                         rset.getInt("iHoras"),
                  });
                 
     
              }
             
          } catch (SQLException ex) {
              Logger.getLogger(practica11.class.getName()).log(Level.SEVERE, null, ex);
          }
     }
     

     
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        btnActualizar = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "ID", "Sistem OP", "Progra", "Diseño", "Admon", "Horas"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);

        btnActualizar.setText("Actualizar");
        btnActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarActionPerformed(evt);
            }
        });

        btnEditar.setText("Editar");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnGuardar.setText("Guardar");
        btnGuardar.setEnabled(false);
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 372, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(167, 167, 167)
                        .addComponent(btnActualizar)))
                .addContainerGap(33, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(btnEditar)
                .addGap(18, 18, 18)
                .addComponent(btnCancelar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnGuardar)
                .addGap(18, 18, 18)
                .addComponent(btnEliminar)
                .addGap(52, 52, 52))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEditar)
                    .addComponent(btnEliminar)
                    .addComponent(btnGuardar)
                    .addComponent(btnCancelar))
                .addGap(59, 59, 59)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(btnActualizar)
                .addContainerGap(25, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed
        this.consultar();
    }//GEN-LAST:event_btnActualizarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
          int renglon = this.jTable1.getSelectedRow();
          String id = "";
          Statement stmt;
          
          if(renglon>=0){
              id = tm.getValueAt(renglon,0).toString();
              try{
                  stmt = conn.createStatement();
                  
                  stmt.execute("DELETE FROM respuestas WHERE id = "+id);
                  this.consultar();
              } catch(SQLException ex){
                  Logger.getLogger(practica11.class.getName()).log(Level.SEVERE,null,ex);
              }
          }else{
              JOptionPane.showMessageDialog(rootPane, "SELECCIONE un registro a eliminar",
                      "Error",
                      JOptionPane.ERROR_MESSAGE);
          }
         
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
         int renglon = this.jTable1.getSelectedRow();
         int columna= this.jTable1.getSelectedColumn();
         
         String id = "";
                    
          if(renglon>=0){
              this.rengSel=renglon;
              this.btnGuardar.setEnabled(true);
              this.btnEditar.setEnabled(false);
              this.btnEliminar.setEnabled(true);
              
              if (this.jTable1.editCellAt(renglon,columna)){
                  
                  Component editor = this.jTable1.getEditorComponent();
                  editor.requestFocusInWindow();
                  
                  System.out.println("Se habilito la edicion de la columna");
              }
              //this.jTable1.editCellAt(renglon,2);
              //this.jTable1.editCellAt(renglon,3);
              //this.jTable1.editCellAt(renglon,4);
              //this.jTable1.editCellAt(renglon,5);
             
              
          }else{
              JOptionPane.showMessageDialog(rootPane, "Seleccione un registro a editar",
                      "Error",
                      JOptionPane.ERROR_MESSAGE);
          }
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
              this.btnGuardar.setEnabled(false);
              this.btnEditar.setEnabled(true);
              this.btnEliminar.setEnabled(true);
              
              
              String sId;
              String sSisOper;
              String sProgra;
              String sDiseno;
              String sAdmon;
              String iHoras;
              
              Statement stmt;
              
              String sSQL = "";
              
              try{
                  stmt=this.conn.createStatement();
                 
                  sId=this.tm.getValueAt(this.rengSel,0).toString();
                  sSisOper=this.tm.getValueAt(this.rengSel,1).toString();
                  sProgra=this.tm.getValueAt(this.rengSel,2).toString();
                  sDiseno=this.tm.getValueAt(this.rengSel,3).toString();
                  sAdmon=this.tm.getValueAt(this.rengSel,4).toString();
                  iHoras=this.tm.getValueAt(this.rengSel,5).toString();
                  
                  sSQL = String.format("UPDATE respuestas SET sSisOper='%s',cProgra='%s',cDiseno='%s',cAdmon='%s',iHoras= %s WHERE id=%s",
                          sSisOper,sProgra,sDiseno,sAdmon,iHoras,sId);
                  
                  stmt.execute(sSQL);
                  
              }catch(SQLException ex){
                  Logger.getLogger(practica11.class.getName()).log(Level.SEVERE,null,ex);
              }
              
              
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        this.btnGuardar.setEnabled(false);
this.btnCancelar.setEnabled(false);
this.btnEditar.setEnabled(true);
this.btnEliminar.setEnabled(true);
this.btnActualizar.setEnabled(true);

 if (this.jTable1.getCellEditor() != null) {
this.jTable1.getCellEditor().cancelCellEditing();
}
    }//GEN-LAST:event_btnCancelarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(practica11.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(practica11.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(practica11.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(practica11.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new practica11().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
