package Ejemplos;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.*;

public class Practica extends JFrame  {
    
    JButton btnCerrar;
    
    Practica(){
    this.setSize(300, 200);
    this.setTitle("Practica 1");
    
    btnCerrar = new JButton("Cerrar");
    MiActionListener ml = new MiActionListener();
    
    btnCerrar.addActionListener(ml);
    
    this.add(btnCerrar);
    }
    
    public static void main(String args[]){
        Practica p = new Practica();
        p.setVisible(true);
    }
}

 

class MiActionListener implements ActionListener{

 

    @Override
    public void actionPerformed(ActionEvent e) {
    System.exit(0);
    }
  
    
}