package Hilos;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Ejemplo {
    public static void main(String args[]){
     Thread th1 = new Thread(new HiloRunnable());
     Thread th2 = new HiloTread();
     
     th1.start(); th2.start();
     
        try {
            th1.join();
            th2.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(Ejemplo.class.getName()).log(Level.SEVERE, null, ex);
        }
        
     System.out.println("La Ejecucion del Main Finalizo");
    }   
}


class HiloRunnable implements Runnable{
    public void run() {
       for(int i=0; i<20; i++){
           System.out.println("BUENOS DIAS: "+i);
           try {
               Thread.sleep(1000);
           } catch (InterruptedException ex) {
               Logger.getLogger(HiloRunnable.class.getName()).log(Level.SEVERE, null, ex);
           }
       }
       System.out.println("Termino el Hilo HIloRunnable");
    }
    
}


class  HiloTread extends Thread{
    public void run(){
        for(int i=100; i<120; i++){
        System.out.println("BUENAS NOCHES: "+i);
    try {
               Thread.sleep(2000);
           } catch (InterruptedException ex) {
               Logger.getLogger(HiloRunnable.class.getName()).log(Level.SEVERE, null, ex);
           }
        }
        System.out.println("Termino el Hilo HIloTread");
    }

}